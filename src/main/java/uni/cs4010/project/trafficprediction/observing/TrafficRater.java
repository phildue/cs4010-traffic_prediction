package uni.cs4010.project.trafficprediction.observing;

import uni.cs4010.project.trafficprediction.mapmatching.roadsegment.RoadSegment;

/**
 * Created by phil on 15.10.16.
 */
public interface TrafficRater {

    StateMap.State getState(RoadSegment roadSegment);
}
