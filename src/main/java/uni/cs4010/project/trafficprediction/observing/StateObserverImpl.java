package uni.cs4010.project.trafficprediction.observing;

import uni.cs4010.project.trafficprediction.mapmatching.roadmap.RoadMap;

/**
 * Created by phil on 15.10.16.
 */
public class StateObserverImpl implements StateObserver {

    private TrafficRater rater;

    public StateObserverImpl(TrafficRater rater) {
        this.rater = rater;
    }

    @Override
    public StateMap observe(RoadMap roadMap) {

        StateMap stateMap = new StateMap();

        roadMap.getRoadSegments()
                .forEach(roadSegment
                        -> stateMap.put(roadSegment.getId(),rater.getState(roadSegment)));
        return stateMap;
    }


}
