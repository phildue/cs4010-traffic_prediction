package uni.cs4010.project.trafficprediction.observing;

import uni.cs4010.project.trafficprediction.mapmatching.roadmap.RoadMap;

/**
 * Created by phil on 15.10.16.
 */
public interface StateObserver {

    StateMap observe(RoadMap roadMap);
}
