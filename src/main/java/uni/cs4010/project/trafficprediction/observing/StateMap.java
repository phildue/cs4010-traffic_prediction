package uni.cs4010.project.trafficprediction.observing;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by phil on 15.10.16.
 */
public class StateMap {

    private HashMap<Long,State> stateMap = new LinkedHashMap<>();

    public void put(Long roadSegmentId,State state){
        stateMap.put(roadSegmentId,state);
    }

    public Map<Long,State> getMap(){
        return stateMap;
    }

    public String toString(){
        StringBuilder s = new StringBuilder();
        stateMap.keySet().forEach(segmentID ->
                s.append(segmentID).append(":").append(stateMap.get(segmentID)).append("\n"));
        return s.toString();
    }

    public enum State{
        UNKNOWN,
        CONGESTED,
        UNCONGESTED
    }
}
