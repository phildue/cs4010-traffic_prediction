package uni.cs4010.project.trafficprediction.observing;

import uni.cs4010.project.trafficprediction.dataset.CarData;
import uni.cs4010.project.trafficprediction.mapmatching.roadsegment.RoadSegment;

import java.util.List;

/**
 * Created by phil on 15.10.16.
 */
public class TrafficRaterImpl implements TrafficRater {
    @Override
    public StateMap.State getState(RoadSegment roadSegment) {
        if(roadSegment.getCars().isEmpty()){
            return StateMap.State.UNKNOWN;
        }else{
            double avgSpeed = calcAvgSpeed(roadSegment.getCars());
            if(avgSpeed < 0.5 * roadSegment.getSpeedLimit()){
                return StateMap.State.CONGESTED;
            }else{
                return StateMap.State.UNCONGESTED;
            }
        }
    }

    private double calcAvgSpeed(List<CarData> cars){
        double sum = 0;
        for (CarData car:
                cars) {
            sum += car.getSpeed();
        }
        return sum/cars.size();
    }
}
