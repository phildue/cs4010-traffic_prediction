package uni.cs4010.project.trafficprediction.runner;

import javaslang.collection.Seq;
import uni.cs4010.project.trafficprediction.dataset.DataImporter;
import uni.cs4010.project.trafficprediction.dataset.DataRow;

public class TimeBucketing {

    private static final String DATASET_DIRECTORY = "./src/main/resource/cardata/";
    private static final String OUTPUT_DIRECTORY = "./src/main/resource/cardata/timebuckets";
    private static final long TIMEBUCKET_SIZE = 1000; // DDHHMMSS

    public static void main(String... args){
        DataImporter dataImporter = new DataImporter(DATASET_DIRECTORY);

        dataImporter
                .getDataFilePaths()
                .forEach(path ->
                    dataImporter.getData(path)
                            .groupBy(x -> x.timestamp - x.timestamp % TIMEBUCKET_SIZE)
                            .filter(x -> !x._2.isEmpty())
                            .forEach(bucket -> saveBucket(bucket._1, bucket._2))
                );
    }

    ///

    private static void saveBucket(Long bucket, Seq<DataRow> bucketData) {
        String filename = OUTPUT_DIRECTORY + "/" + bucket + ".csv";
        DataImporter dataImporter = new DataImporter(DATASET_DIRECTORY);
        dataImporter.saveData(filename, bucketData, true);
    }
}
