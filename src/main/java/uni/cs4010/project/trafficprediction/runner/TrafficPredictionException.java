package uni.cs4010.project.trafficprediction.runner;

/**
 * Created by phil on 15.10.16.
 */
public class TrafficPredictionException extends RuntimeException{
    public TrafficPredictionException(String msg){
        super(msg);
    }
}
