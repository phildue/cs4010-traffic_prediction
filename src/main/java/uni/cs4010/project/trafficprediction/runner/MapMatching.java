package uni.cs4010.project.trafficprediction.runner;

import uni.cs4010.project.trafficprediction.dataset.CarTrace;
import uni.cs4010.project.trafficprediction.dataset.DataImporter;
import uni.cs4010.project.trafficprediction.dataset.TraceFilter;
import uni.cs4010.project.trafficprediction.dataset.TraceFilterImpl;
import uni.cs4010.project.trafficprediction.mapmatching.MapMatcher;
import uni.cs4010.project.trafficprediction.mapmatching.MapMatcherImpl;
import uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher.GpsLocation;
import uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher.GpsMatcher;
import uni.cs4010.project.trafficprediction.mapmatching.graphhopper.EdgeFactory;
import uni.cs4010.project.trafficprediction.mapmatching.graphhopper.GraphHopper;
import uni.cs4010.project.trafficprediction.mapmatching.roadmap.RoadMap;
import uni.cs4010.project.trafficprediction.mapmatching.roadsegment.RoadSegmentFactory;
import uni.cs4010.project.trafficprediction.mapmatching.roadsegment.RoadSegmentFactoryImpl;
import uni.cs4010.project.trafficprediction.observing.StateMap;
import uni.cs4010.project.trafficprediction.observing.StateObserver;
import uni.cs4010.project.trafficprediction.observing.StateObserverImpl;
import uni.cs4010.project.trafficprediction.observing.TrafficRaterImpl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

/**
 * Created by phil on 09.10.16.
 */
public class MapMatching {

    public static final String MAP_DIR = "src/main/resource/maps/";
    public static final String MAP_NAME = "china-latest.osm.pbf";
    public static final String DATASET_DIRECTORY = "./src/main/resource/cardata/timebuckets";
    public static final String OUTPUT_DIR = "target/";
    public static final String OUTPUT_FILE = "statemap";

    public static final String GRAPHHOPPER_LOCATION = "target/graphhopper";
    public static final GpsLocation SPAN_VECTOR = new GpsLocation(0.900,0.900);
    public static final String FLAG_ENCODER_FLAG = "CAR";
    private static final Double BEIJING_CENTER_LON = 39.921829;
    private static final Double BEIJING_CENTER_LAT = 116.390378;
    public static final GpsLocation MAP_CENTER = new GpsLocation(BEIJING_CENTER_LON,BEIJING_CENTER_LAT);

    public static void main(String... args){

        GraphHopper graphHopper = new GraphHopper(GRAPHHOPPER_LOCATION,MAP_DIR,MAP_NAME);
        GpsMatcher gpsMatcher = graphHopper.createGpsMatcher();

        RoadSegmentFactory roadSegmentFactory = new RoadSegmentFactoryImpl(
                MAP_CENTER,
                SPAN_VECTOR,
                graphHopper.getFlagEncoder(FLAG_ENCODER_FLAG),
                new EdgeFactory(graphHopper.getGraph()));

        MapMatcher mapMatcher = new MapMatcherImpl(gpsMatcher,roadSegmentFactory);
        DataImporter dataImporter = new DataImporter(DATASET_DIRECTORY);
        TraceFilter traceFilter = new TraceFilterImpl();
        StateObserver observer = new StateObserverImpl(new TrafficRaterImpl());

        dataImporter.getDataFilePaths().forEach(path ->  {

            String fileName = path.getFileName().toString();
            Long timestamp = Long.parseLong(fileName.substring(0,fileName.lastIndexOf(".")));

            Map<Long,CarTrace> carTraces = traceFilter.getGpsTraces(dataImporter.getData(path));
            System.out.println(String.format("Data of [%d] vehicles read",carTraces.values().size()));

            RoadMap roadMap = mapMatcher.match(carTraces);
            System.out.println(String.format("Total of [%d] observations matched to [%d] road segments",roadMap.getTotalCars(),roadMap.getRoadSegments().size()));

            StateMap stateMap = observer.observe(roadMap);

            try {
                String outputFilePath = OUTPUT_DIR + OUTPUT_FILE + timestamp;
                FileWriter fw = new FileWriter(new File(outputFilePath));
                fw.write(stateMap.toString());
                System.out.println(String.format("Writing state map for %d in file %s",timestamp,outputFilePath));
            } catch (IOException e) {
                e.printStackTrace();
            }

        });



    }

}
