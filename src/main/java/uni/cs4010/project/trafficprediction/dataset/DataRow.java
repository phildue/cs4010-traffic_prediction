package uni.cs4010.project.trafficprediction.dataset;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DataRow {

    @JsonProperty("taxiId")
    public String taxiId;

    @JsonProperty("messageType")
    public int messageType;

    @JsonProperty("x1")
    public int x1;

    @JsonProperty("timestamp")
    public Long timestamp;

    @JsonProperty("longitude")
    public double longitude;

    @JsonProperty("latitude")
    public double latitude;

    @JsonProperty("speed")
    public int speed;

    @JsonProperty("direction")
    public int direction;

    @JsonProperty("x2")
    public int x2;

    @JsonCreator
    public DataRow(
            @JsonProperty("taxiId")String taxiId,
            @JsonProperty("messageType")int messageType,
            @JsonProperty("x1")int x1,
            @JsonProperty("timestamp")Long timestamp,
            @JsonProperty("longitude")double longitude,
            @JsonProperty("latitude")double latitude,
            @JsonProperty("speed")int speed,
            @JsonProperty("direction")int direction,
            @JsonProperty("x2")int x2) {

        this.taxiId = taxiId;
        this.messageType = messageType;
        this.x1 = x1;
        this.timestamp = timestamp;
        this.longitude = longitude;
        this.latitude = latitude;
        this.speed = speed;
        this.direction = direction;
        this.x2 = x2;
    }

    public enum MessageType {
        PICK_UP, START_BREAK, END_BREAK, DROP_OFF
    }
}
