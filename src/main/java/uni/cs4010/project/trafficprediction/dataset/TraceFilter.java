package uni.cs4010.project.trafficprediction.dataset;

import javaslang.collection.Seq;

import java.util.Map;

/**
 * Created by phil on 15.10.16.
 */
public interface TraceFilter {

    Map<Long,CarTrace> getGpsTraces(Seq<DataRow> dataRows);
}
