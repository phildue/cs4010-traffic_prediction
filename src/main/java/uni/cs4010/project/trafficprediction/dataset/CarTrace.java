package uni.cs4010.project.trafficprediction.dataset;

import uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher.GpsLocation;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by phil on 15.10.16.
 */
public class CarTrace {

    private List<DataRow> carData;

    public CarTrace(List<DataRow> carData) {
        this.carData = carData;
    }
    public List<GpsLocation> getGpsTrace(){
        return carData.stream()
                .map(dataEntry -> new GpsLocation(dataEntry.longitude,dataEntry.latitude,dataEntry.timestamp))
                .collect(Collectors.toList());
    }

    public List<CarData> getCarData(){
        return carData.stream()
                .map(dataEntry -> new CarData(Long.parseLong(dataEntry.taxiId),dataEntry.speed,dataEntry.direction))
                .collect(Collectors.toList());
    }
}
