package uni.cs4010.project.trafficprediction.dataset;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import javaslang.collection.List;
import javaslang.collection.Seq;
import javaslang.control.Try;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

public class DataImporter {

    private static final char CSV_SEPARATOR = ',';
    private String datasetDirectory;
    private CsvSchema schema;

    public DataImporter(String datasetDirectory){
        this.datasetDirectory = datasetDirectory;
        this.schema = CsvSchema.builder()
                .addColumn("taxiId", CsvSchema.ColumnType.STRING)
                .addColumn("messageType", CsvSchema.ColumnType.NUMBER)
                .addColumn("x1", CsvSchema.ColumnType.NUMBER)
                .addColumn("timestamp", CsvSchema.ColumnType.NUMBER)
                .addColumn("longitude", CsvSchema.ColumnType.NUMBER)
                .addColumn("latitude", CsvSchema.ColumnType.NUMBER)
                .addColumn("speed", CsvSchema.ColumnType.NUMBER)
                .addColumn("direction", CsvSchema.ColumnType.NUMBER)
                .addColumn("x2", CsvSchema.ColumnType.NUMBER)
                .build().withColumnSeparator(CSV_SEPARATOR);
    }

    ///

    public Seq<DataRow> getData(Path path) {
        System.out.println("Getting Data for " + path);
        try {
            return readFile(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return List.empty();
    }

    public Seq<Path> getDataFilePaths() {
        try {
            return walkFolder(datasetDirectory);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return List.empty();
    }

    public Boolean saveData(String path, Seq<DataRow> dataRows, Boolean append) {
        System.out.println("Saving Data for " + path);
        try {
            saveCsv(path, dataRows, append);
            return Boolean.TRUE;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Boolean.FALSE;
    }

    ///

    private Seq<DataRow> readFile(Path path) throws IOException {
        ObjectMapper mapper = new CsvMapper();

        return List.ofAll(Files.lines(path).collect(Collectors.toList()))
                .map(csv ->
                        Try.of(() ->
                                (DataRow) mapper.readerFor(DataRow.class).with(schema).readValue(csv)
                        ).getOrElse(new DataRow("NULL",0,0,0L,0,0,0,0,0))
                );
    }

    private Seq<Path> walkFolder(String directoryPathString) throws IOException {
        return List.ofAll(
                Files.walk(Paths.get(directoryPathString))
                        .filter(Files::isRegularFile)
                        .filter(x -> x.getFileName().toString().contains(".txt") || x.getFileName().toString().contains(".csv"))
                        .collect(Collectors.toList())
        );
    }

    private void saveCsv(String path, Seq<DataRow> dataRows, Boolean append) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(path, append);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, "UTF-8");
        BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);

        for (String data : dataRows.map(this::toCsvString).toJavaList()) {
            bufferedWriter.write(data);
            bufferedWriter.newLine();
        }

        bufferedWriter.flush();
        bufferedWriter.close();
    }

    private String toCsvString(DataRow x) {
        return x.taxiId +","+ x.messageType +","+ x.x1 +","+ x.timestamp +","+ x.longitude +","+ x.latitude +","+ x.speed +","+ x.direction +","+ x.x2;
    }
}
