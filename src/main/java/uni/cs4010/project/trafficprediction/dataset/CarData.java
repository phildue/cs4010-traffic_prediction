package uni.cs4010.project.trafficprediction.dataset;

/**
 * Created by phil on 08.10.16.
 */
public class CarData {

    private Long id;
    private int speed;
    private double direction;

    public CarData(Long id, int speed, double direction) {
        this.id = id;
        this.speed = speed;
        this.direction = direction;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public Long getId() {
        return id;
    }

    public double getDirection() {
        return direction;
    }
}
