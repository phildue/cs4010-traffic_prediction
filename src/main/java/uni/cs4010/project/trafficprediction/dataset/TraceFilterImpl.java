package uni.cs4010.project.trafficprediction.dataset;

import javaslang.collection.Seq;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by phil on 15.10.16.
 */
public class TraceFilterImpl implements TraceFilter{

    public Map<Long,CarTrace> getGpsTraces(Seq<DataRow> dataRows){
        HashMap<Long,CarTrace> gpsTraces = new HashMap<>();

        dataRows.forEach(dataRow -> {
            Long carId = Long.parseLong(dataRow.taxiId);
            if(!gpsTraces.containsKey(carId)){
                gpsTraces.put(carId, getCarTrace(dataRows,carId));
            }
        });
        return gpsTraces;
    }

    private CarTrace getCarTrace(Seq<DataRow> dataRows, long carId){

        return new CarTrace(dataRows.toJavaStream()
                .filter(dataRow -> dataRow.taxiId.equals(Long.toString(carId))).collect(Collectors.toList()));
    }
}
