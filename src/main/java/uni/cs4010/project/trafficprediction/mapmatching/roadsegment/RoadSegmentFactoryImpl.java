package uni.cs4010.project.trafficprediction.mapmatching.roadsegment;

import com.graphhopper.routing.util.FlagEncoder;
import uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher.GpsLocation;
import uni.cs4010.project.trafficprediction.mapmatching.graphhopper.EdgeFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by phil on 09.10.16.
 */
public class RoadSegmentFactoryImpl implements RoadSegmentFactory {

    private EdgeFactory edgeFactory;
    private FlagEncoder encoder;
    private GpsLocation center;
    private GpsLocation span;

    public RoadSegmentFactoryImpl(GpsLocation center, GpsLocation span, FlagEncoder encoder, EdgeFactory edgeFactory){
        this.encoder = encoder;
        this.edgeFactory = edgeFactory;
        this.center = center;
        this.span = span;
    }

    public List<RoadSegment> getRoadSegments() {

        // Here we extract the data of the osm map so far only location and max speed are include
        // if we need more data like directions this is the place to modify
        // more data can be accessed by implementing an own FlagEncoder

        return edgeFactory.getEdgesWithinSpan(center, span).stream()
                .map(edge -> new RoadSegmentImpl(
                edge,
                encoder.getMaxSpeed()) {
        }).collect(Collectors.toCollection(LinkedList::new));
    }


}
