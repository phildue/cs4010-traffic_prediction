package uni.cs4010.project.trafficprediction.mapmatching;

import uni.cs4010.project.trafficprediction.dataset.CarTrace;
import uni.cs4010.project.trafficprediction.mapmatching.roadmap.RoadMap;

import java.util.Map;

/**
 * Created by phil on 15.10.16.
 */
public interface MapMatcher {

    RoadMap match(Map<Long,CarTrace> carTraces);

}
