package uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher;

import java.util.List;

/**
 * Created by phil on 09.10.16.
 */
public interface GpsMatcher {

    List<Long> getPassedEdges(GpsLocation from, GpsLocation to);
}
