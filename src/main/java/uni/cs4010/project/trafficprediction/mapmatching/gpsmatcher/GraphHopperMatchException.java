package uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher;

import uni.cs4010.project.trafficprediction.runner.TrafficPredictionException;

/**
 * Created by phil on 15.10.16.
 */
public class GraphHopperMatchException extends TrafficPredictionException {
    public GraphHopperMatchException(String msg) {
        super(msg);
    }
}
