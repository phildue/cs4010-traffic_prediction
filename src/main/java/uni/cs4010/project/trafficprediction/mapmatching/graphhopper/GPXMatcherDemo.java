package uni.cs4010.project.trafficprediction.mapmatching.graphhopper;

import com.graphhopper.matching.*;
import com.graphhopper.storage.GraphHopperStorage;
import com.graphhopper.storage.NodeAccess;
import com.graphhopper.storage.index.LocationIndexTree;
import com.graphhopper.util.GPXEntry;

import java.util.Arrays;
import java.util.List;

import static uni.cs4010.project.trafficprediction.runner.MapMatching.GRAPHHOPPER_LOCATION;

/**
 * Created by phil on 01.10.16.
 */
public class GPXMatcherDemo {

    private static final String GPX_TEST_DIR = "src/test/resources/gpx/";
    private static final List<String> GPX_TEST_SET = Arrays.asList(
            "test_delft1.gpx", "test_delft2.gpx","test_delft3.gpx", "test_delft4.gpx");
    private static final String MAP_TEST_DIR = "";
    private static final String MAP_TEST_NAME = "";
    private static GraphHopperStorage graph;
    private static com.graphhopper.GraphHopper hopper;

    public static void main(String... args){
        printTrace();
    }

    public static void printTrace(){
        GraphHopper hopperInitializer = new GraphHopper(GRAPHHOPPER_LOCATION,MAP_TEST_DIR,MAP_TEST_NAME);
        graph = hopperInitializer.getGraph();
        hopper = hopperInitializer.getHopper();
        LocationIndexMatch locationIndex = new LocationIndexMatch(graph,
                (LocationIndexTree) hopper.getLocationIndex());
        MapMatching mapMatching = new MapMatching(graph, locationIndex,hopperInitializer.getEncoder());

// do the actual matching, get the GPX entries from a file or via stream
        List<GPXEntry> inputGPXEntries = new GPXFile().doImport(GPX_TEST_DIR + GPX_TEST_SET.get(0)).getEntries();
        MatchResult mr = mapMatching.doWork(inputGPXEntries);

// return GraphHopper edges with all associated GPX entries
        List<EdgeMatch> matches = mr.getEdgeMatches();
// now do something with the edges like storing the edgeIds or doing fetchWayGeometry etc
        matches.get(0).getEdgeState();

        NodeAccess nodeAccess = graph.getNodeAccess();
        for(EdgeMatch m : matches){
            //System.out.println(m);
            int baseNodeId = m.getEdgeState().getBaseNode();
            int adjNodeId = m.getEdgeState().getAdjNode();
            System.out.println(String.format("From: %04f | %04f",nodeAccess.getLon(baseNodeId),nodeAccess.getLat(baseNodeId)));
            System.out.println(String.format("To  : %04f | %04f",nodeAccess.getLon(adjNodeId),nodeAccess.getLat(adjNodeId)));
            System.out.println(String.format("Diff: %04f | %04f",nodeAccess.getLon(adjNodeId)-nodeAccess.getLon(baseNodeId),
                    nodeAccess.getLat(adjNodeId)-nodeAccess.getLat(baseNodeId)));

            //System.out.println(m.getEdgeState().getBaseNode());
            //System.out.println(m.getEdgeState().getAdjNode());
        }


    }
}
