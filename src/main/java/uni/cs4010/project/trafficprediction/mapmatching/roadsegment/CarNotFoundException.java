package uni.cs4010.project.trafficprediction.mapmatching.roadsegment;

import uni.cs4010.project.trafficprediction.runner.TrafficPredictionException;

/**
 * Created by phil on 09.10.16.
 */
public class CarNotFoundException extends TrafficPredictionException {
    public CarNotFoundException(String msg){
        super(msg);
    }
}
