package uni.cs4010.project.trafficprediction.mapmatching.graphhopper;

import com.graphhopper.storage.Graph;
import com.graphhopper.storage.NodeAccess;
import com.graphhopper.util.EdgeIterator;
import uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher.GpsLocation;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Created by phil on 01.10.16.
 * We want a class that
 * receives the edgeIterator of a mapmatching and a certain area within that mapmatching
 * returns only the edgeIterator that are in that area
 */
public class EdgeFactory {

    private NodeAccess nodeAccess;
    private Graph graph;

    public EdgeFactory(Graph graph){
        this.graph = graph;
        this.nodeAccess = graph.getNodeAccess();
    }


    public Collection<Edge> getEdgesWithinSpan(GpsLocation center, GpsLocation span){
        return getEdgesWithinMinMax(
                calcMin(center,span),
                calcMax(center,span)
        );
    }

    public Collection<Edge> getEdgesWithinMinMax(GpsLocation min, GpsLocation max){

        Collection<Edge> edges = new LinkedList<>();

        EdgeIterator edgeIterator = graph.getAllEdges();

        while(edgeIterator.next()){

            GpsLocation baseNode = new GpsLocation(nodeAccess.getLat(edgeIterator.getBaseNode()),nodeAccess.getLon(edgeIterator.getBaseNode()));
            GpsLocation adjNode = new GpsLocation(nodeAccess.getLat(edgeIterator.getAdjNode()),nodeAccess.getLon(edgeIterator.getAdjNode()));

            if( (baseNode.smallerThan(max) && baseNode.greaterThan(min)
                && adjNode.smallerThan(max) && adjNode.greaterThan(min))){

                edges.add(new Edge((long)edgeIterator.getEdge(),baseNode,adjNode));
            }
        }

        return edges;
    }

    private GpsLocation calcMin(GpsLocation center, GpsLocation span){
        return new GpsLocation(center.getLon()-span.getLon(),center.getLat()-span.getLat());
    }

    private GpsLocation calcMax(GpsLocation center, GpsLocation span){
        return new GpsLocation(center.getLon()+span.getLon(),center.getLat()+span.getLat());
    }
}
