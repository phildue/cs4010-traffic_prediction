package uni.cs4010.project.trafficprediction.mapmatching.roadmap;

import uni.cs4010.project.trafficprediction.dataset.CarData;
import uni.cs4010.project.trafficprediction.mapmatching.roadsegment.RoadSegment;

import java.util.List;

/**
 * Created by phil on 09.10.16.
 */
public interface RoadMap {

    void update(CarData carData, Long edgeId) throws RoadSegmentNotFoundException;
    List<RoadSegment> getRoadSegments();
    RoadSegment getRoadSegment(Long id);
    int getTotalCars();
}
