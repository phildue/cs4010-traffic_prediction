package uni.cs4010.project.trafficprediction.mapmatching;

import uni.cs4010.project.trafficprediction.dataset.CarData;
import uni.cs4010.project.trafficprediction.dataset.CarTrace;
import uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher.GpsLocation;
import uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher.GpsMatcher;
import uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher.GraphHopperMatchException;
import uni.cs4010.project.trafficprediction.mapmatching.roadmap.RoadMap;
import uni.cs4010.project.trafficprediction.mapmatching.roadmap.RoadMapImpl;
import uni.cs4010.project.trafficprediction.mapmatching.roadmap.RoadSegmentNotFoundException;
import uni.cs4010.project.trafficprediction.mapmatching.roadsegment.RoadSegmentFactory;

import java.util.Map;

/**
 * Created by phil on 15.10.16.
 */
public class MapMatcherImpl implements MapMatcher {

    private GpsMatcher gpsMatcher;
    private RoadSegmentFactory roadSegmentFactory;

    public MapMatcherImpl(GpsMatcher gpsMatcher, RoadSegmentFactory roadSegmentFactory) {
        this.gpsMatcher = gpsMatcher;
        this.roadSegmentFactory = roadSegmentFactory;
    }

    @Override
    public RoadMap match(Map<Long, CarTrace> carTraces) {

        RoadMap roadMap =  new RoadMapImpl(roadSegmentFactory.getRoadSegments());

        carTraces.forEach((carId, carTrace) -> {

            for(int i=0;i<carTrace.getGpsTrace().size()-1;i++){

                CarData carData = carTrace.getCarData().get(i);
                GpsLocation from = carTrace.getGpsTrace().get(i);
                GpsLocation to = carTrace.getGpsTrace().get(i+1);

                try{
                    gpsMatcher.getPassedEdges(from,to).
                            forEach(edgeID ->roadMap.update(carData,edgeID));
                }catch (GraphHopperMatchException e){
                    // We ignore missmatches for now
                }catch (RoadSegmentNotFoundException e2){
                    System.err.println(e2.getMessage());
                }

            }
        });

        return roadMap;
    }



}
