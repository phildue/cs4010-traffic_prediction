package uni.cs4010.project.trafficprediction.mapmatching.roadmap;

import uni.cs4010.project.trafficprediction.dataset.CarData;
import uni.cs4010.project.trafficprediction.mapmatching.roadsegment.RoadSegment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Created by phil on 09.10.16.
 */
public class RoadMapImpl implements RoadMap {

    private HashMap<Long,RoadSegment> roadSegments = new HashMap<>();

    public RoadMapImpl(Collection<RoadSegment> roadSegments){
        for(RoadSegment rs: roadSegments){
            this.roadSegments.put(rs.getId(),rs);
        }

    }

    @Override
    public void update(CarData carData, Long edgeId) throws RoadSegmentNotFoundException {

        RoadSegment roadSegment = getRoadSegment(edgeId);

        roadSegment.update(carData);

    }

    @Override
    public List<RoadSegment> getRoadSegments() {
        return new ArrayList<>(roadSegments.values());
    }

    public RoadSegment getRoadSegment(Long id) {
        if(!roadSegments.containsKey(id))
            throw new RoadSegmentNotFoundException("Road segment with id: ["+ id +"] was not found.");
        return roadSegments.get(id);
    }


    @Override
    public int getTotalCars() {
        int sum = 0;
        for(RoadSegment rs: roadSegments.values()){
            sum += rs.getNumberOfCars();
        }
        return sum;
    }


}
