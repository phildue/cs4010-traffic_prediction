package uni.cs4010.project.trafficprediction.mapmatching.roadsegment;

import java.util.List;

/**
 * Created by phil on 09.10.16.
 */
public interface RoadSegmentFactory {

    List<RoadSegment> getRoadSegments();
}
