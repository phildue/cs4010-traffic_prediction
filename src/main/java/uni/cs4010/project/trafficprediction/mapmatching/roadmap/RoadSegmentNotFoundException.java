package uni.cs4010.project.trafficprediction.mapmatching.roadmap;

import uni.cs4010.project.trafficprediction.runner.TrafficPredictionException;

/**
 * Created by phil on 09.10.16.
 */
public class RoadSegmentNotFoundException extends TrafficPredictionException {
    public RoadSegmentNotFoundException(String msg){
        super(msg);
    }
}
