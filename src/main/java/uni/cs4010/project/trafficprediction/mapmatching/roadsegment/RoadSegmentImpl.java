package uni.cs4010.project.trafficprediction.mapmatching.roadsegment;

import uni.cs4010.project.trafficprediction.dataset.CarData;
import uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher.GpsLocation;
import uni.cs4010.project.trafficprediction.mapmatching.graphhopper.Edge;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by phil on 08.10.16.
 */
public class RoadSegmentImpl implements RoadSegment {

    private Edge edge;
    private Double speedLimit;
    private HashMap<Long,CarData> cars;

    public RoadSegmentImpl(Edge edge, Double speedLimit) {
        this.edge = edge;
        this.speedLimit = speedLimit;
        cars = new HashMap<>();
    }

    public GpsLocation getBaseNode(){
        return edge.getBaseNode();
    }

    public GpsLocation getEndNode(){
        return edge.getAdjNode();
    }

    public int getNumberOfCars() {
        return cars.size();
    }

    public boolean containsCar(Long id){
        return cars.containsKey(id);
    }

    @Override
    public List<CarData> getCars() {
        return new ArrayList<>(cars.values());
    }

    public CarData removeCar(Long id) throws CarNotFoundException {
        if(!cars.containsKey(id))
            throw new CarNotFoundException("Car with ID :[" + id +"] was not found.");
        return cars.remove(id);
    }

    public void update(CarData carData) throws CarNotFoundException {
        cars.put(carData.getId(), carData);
    }

    public CarData getCar(Long id) throws CarNotFoundException {
        if(!cars.containsKey(id))
            throw new CarNotFoundException("Car with ID :[" + id +"] was not found.");
        return cars.get(id);
    }

    public Long getId() {
       return edge.getId();
    }

    public Double getSpeedLimit() {
        return speedLimit;
    }
}
