package uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher;

/**
 * Created by phil on 03.10.16.
 */
public class GpsLocation {

    private double lon, lat;
    private Long timestamp;

    public GpsLocation(double lon, double lat, long timestamp) {
        this.lon = lon;
        this.lat = lat;
        this.timestamp = timestamp;
    }
    public GpsLocation(double lon,double lat){
        this.lon = lon;
        this.lat = lat;
        this.timestamp = null;
    }

    public double getLon() {
        return lon;
    }

    public double getLat() {
        return lat;
    }

    public boolean smallerThan(GpsLocation that){
        return (this.getLat() < that.getLat()) && (this.getLon() < that.getLon());
    }

    public boolean greaterThan(GpsLocation that){
        return (this.getLat() > that.getLat()) && (this.getLon() > that.getLon());
    }
    public String toString(){
        return String.format("%04f,%04f", lon, lat);
    }

    public Long getTimestamp() {
        return timestamp;
    }
}
