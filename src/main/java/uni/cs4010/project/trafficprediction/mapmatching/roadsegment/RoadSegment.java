package uni.cs4010.project.trafficprediction.mapmatching.roadsegment;

import uni.cs4010.project.trafficprediction.dataset.CarData;
import uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher.GpsLocation;

import java.util.List;

/**
 * Created by phil on 09.10.16.
 */
public interface RoadSegment {

    void update(CarData carData) throws CarNotFoundException;
    CarData getCar(Long id) throws CarNotFoundException;
    CarData removeCar(Long id) throws CarNotFoundException;

    boolean containsCar(Long id);
    List<CarData> getCars();
    Long getId();
    Double getSpeedLimit();
    GpsLocation getBaseNode();

    GpsLocation getEndNode();

    int getNumberOfCars();

}
