package uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher;

import com.graphhopper.matching.MapMatching;
import com.graphhopper.matching.MatchResult;
import com.graphhopper.util.GPXEntry;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by phil on 09.10.16.
 */
public class GpsMatcherImpl implements GpsMatcher {

    private MapMatching mapMatching;

    public GpsMatcherImpl(MapMatching mapMatching){
        this.mapMatching = mapMatching;
    }


    @Override
    public List<Long> getPassedEdges(GpsLocation from, GpsLocation to) {
         return matchGps(Arrays.asList(from,to));
    }


    public List<Long> matchGps(List<GpsLocation> gpsTrace) {

        return matchGPX(gpsTrace.stream()
                .map(gps -> new GPXEntry(gps.getLat(), gps.getLon(), gps.getTimestamp()))
                .collect(Collectors.toList())
        );
    }

     private List<Long> matchGPX(List<GPXEntry> gpxEntries) {

        MatchResult matchResult = tryMatch(gpxEntries);

        return matchResult.getEdgeMatches().stream()
                .map(edgeMatch -> Integer.toUnsignedLong(edgeMatch.getEdgeState().getEdge()))
                .collect(Collectors.toList());
    }

    private MatchResult tryMatch(List<GPXEntry> gpxEntries) {
        try{
            return mapMatching.doWork(gpxEntries);
        }catch (Exception e){
            throw new GraphHopperMatchException(e.getMessage());
        }
    }



}
