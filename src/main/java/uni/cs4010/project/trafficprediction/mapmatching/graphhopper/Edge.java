package uni.cs4010.project.trafficprediction.mapmatching.graphhopper;

import uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher.GpsLocation;

/**
 * Created by phil on 03.10.16.
 */
public class Edge {
    private  Long id;
    private GpsLocation baseNode,adjNode;

    public Edge(Long id, GpsLocation baseNode, GpsLocation adjNode) {
        this.id = id;
        this.baseNode = baseNode;
        this.adjNode = adjNode;
    }

    public GpsLocation getBaseNode() {
        return baseNode;
    }

    public GpsLocation getAdjNode() {
        return adjNode;
    }

    public Long getId() {
        return id;
    }

    public String toString(){
        return String.format("Id:\t %04d\n" +
                "               BaseNode: \t %s\n" +
                "               AdjNode: \t %s",id,baseNode.toString(),adjNode.toString());
    }
}
