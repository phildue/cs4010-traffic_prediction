package uni.cs4010.project.trafficprediction.mapmatching.graphhopper;

import com.graphhopper.matching.LocationIndexMatch;
import com.graphhopper.matching.MapMatching;
import com.graphhopper.routing.util.CarFlagEncoder;
import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.storage.GraphHopperStorage;
import com.graphhopper.storage.index.LocationIndexTree;
import uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher.GpsMatcher;
import uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher.GpsMatcherImpl;

/**
 * Created by phil on 01.10.16.
 */
public class GraphHopper {


    private GraphHopperStorage graph;
    private com.graphhopper.GraphHopper hopper;
    private CarFlagEncoder encoder;
    public GraphHopper(String graphHopperLocation, String mapDir, String mapName){
        // import OpenStreetMap data
        hopper = new com.graphhopper.GraphHopper();
        hopper.setOSMFile(mapDir + mapName);
        hopper.setGraphHopperLocation(graphHopperLocation);
        encoder = new CarFlagEncoder();
        hopper.setEncodingManager(new EncodingManager(encoder));
        hopper.getCHFactoryDecorator().setEnabled(false);
        hopper.importOrLoad();

// create MapMatching object, can and should be shared accross threads

        graph = hopper.getGraphHopperStorage();

    }

    public GraphHopperStorage getGraph(){
        return graph;
    }

    public com.graphhopper.GraphHopper getHopper(){
        return hopper;
    }

    public CarFlagEncoder getEncoder(){
        return encoder;
    }

    public GpsMatcher createGpsMatcher(){
        MapMatching mapMatching = new MapMatching(getGraph(),
                new LocationIndexMatch(getGraph(),(LocationIndexTree) getHopper().getLocationIndex()), getEncoder());
        mapMatching.setMaxVisitedNodes(1000);
        return new GpsMatcherImpl(mapMatching);
    }

    public FlagEncoder getFlagEncoder(String name){
       return getGraph().getEncodingManager().getEncoder(name);
    }
}
