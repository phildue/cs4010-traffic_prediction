package uni.cs4010.project.trafficprediction.mapmatching.roadsegment;

import org.junit.Before;
import org.junit.Test;
import uni.cs4010.project.trafficprediction.dataset.CarData;
import uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher.GpsLocation;
import uni.cs4010.project.trafficprediction.mapmatching.graphhopper.Edge;

import static org.junit.Assert.assertEquals;

/**
 * Created by phil on 15.10.16.
 */
public class RoadSegmentImplTest {

    private static final Double SPEED_LIMIT = 50.0;
    private static final GpsLocation DUMMY_BASE_NODE = new GpsLocation(0.1,0.2,0L);
    private static final GpsLocation DUMMY_END_NODE = new GpsLocation(0.2,0.1,0L);
    private static final Edge DUMMY_EDGE = new Edge(0L,DUMMY_BASE_NODE,DUMMY_END_NODE);
    private RoadSegmentImpl roadSegment;

    @Before
    public void setUp(){
        roadSegment = new RoadSegmentImpl(DUMMY_EDGE,SPEED_LIMIT);
    }

    @Test
    public void initTest(){

        final CarData DUMMY_CAR = new CarData(0L,20,20.0);
        roadSegment.update(DUMMY_CAR);
        assertEquals(roadSegment.containsCar(DUMMY_CAR.getId()),true);
        assertEquals(roadSegment.getCar(DUMMY_CAR.getId()),DUMMY_CAR);
        assertEquals(roadSegment.getNumberOfCars(),1);
        assertEquals(roadSegment.getSpeedLimit(),SPEED_LIMIT);
        assertEquals(roadSegment.getBaseNode(),DUMMY_BASE_NODE);
        assertEquals(roadSegment.getEndNode(),DUMMY_END_NODE);
        assertEquals(roadSegment.getId(),DUMMY_EDGE.getId());

    }

    @Test(expected = CarNotFoundException.class)
    public void removeTest(){

        final CarData DUMMY_CAR = new CarData(0L,20,20.0);
        roadSegment.update(DUMMY_CAR);
        roadSegment.removeCar(DUMMY_CAR.getId());

        assertEquals(roadSegment.containsCar(DUMMY_CAR.getId()),false);
        roadSegment.getCar(DUMMY_CAR.getId());

    }

}