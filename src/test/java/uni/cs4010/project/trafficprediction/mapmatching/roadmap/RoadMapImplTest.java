package uni.cs4010.project.trafficprediction.mapmatching.roadmap;

import org.junit.Before;
import org.junit.Test;
import uni.cs4010.project.trafficprediction.dataset.CarData;
import uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher.GpsLocation;
import uni.cs4010.project.trafficprediction.mapmatching.graphhopper.Edge;
import uni.cs4010.project.trafficprediction.mapmatching.roadsegment.RoadSegment;
import uni.cs4010.project.trafficprediction.mapmatching.roadsegment.RoadSegmentImpl;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by phil on 15.10.16.
 */
public class RoadMapImplTest {

    private static final Double SPEED_LIMIT = 50.0;
    private static final GpsLocation DUMMY_BASE_NODE = new GpsLocation(0.1,0.2,0L);
    private static final GpsLocation DUMMY_END_NODE = new GpsLocation(0.2,0.1,0L);
    private static final Edge DUMMY_EDGE = new Edge(0L,DUMMY_BASE_NODE,DUMMY_END_NODE);
    private static final RoadSegment DUMMY_SEGMENT = new RoadSegmentImpl(DUMMY_EDGE,SPEED_LIMIT);
    private static final List<RoadSegment> DUMMY_SEGMENTS = Arrays.asList(
            DUMMY_SEGMENT,
            new RoadSegmentImpl(new Edge(1L,DUMMY_BASE_NODE,DUMMY_END_NODE),SPEED_LIMIT),
            new RoadSegmentImpl(new Edge(2L,DUMMY_BASE_NODE,DUMMY_END_NODE),SPEED_LIMIT),
            new RoadSegmentImpl(new Edge(3L,DUMMY_BASE_NODE,DUMMY_END_NODE),SPEED_LIMIT)
    );
    private RoadMapImpl roadMap;

    @Before
    public void setUp(){
        roadMap = new RoadMapImpl(DUMMY_SEGMENTS);
    }

    @Test
    public void initTest(){
        final CarData DUMMY_CAR = new CarData(0L,20,20.0);
        roadMap.update(DUMMY_CAR,0L);
        assertEquals(roadMap.getRoadSegment(DUMMY_EDGE.getId()),DUMMY_SEGMENT);
        assertEquals(roadMap.getTotalCars(),1);
        assertEquals(roadMap.getRoadSegments().size(),4);
    }

}