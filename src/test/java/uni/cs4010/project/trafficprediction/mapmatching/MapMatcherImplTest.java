package uni.cs4010.project.trafficprediction.mapmatching;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import uni.cs4010.project.trafficprediction.dataset.CarTrace;
import uni.cs4010.project.trafficprediction.dataset.DataRow;
import uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher.GpsLocation;
import uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher.GpsMatcher;
import uni.cs4010.project.trafficprediction.mapmatching.graphhopper.Edge;
import uni.cs4010.project.trafficprediction.mapmatching.roadmap.RoadMap;
import uni.cs4010.project.trafficprediction.mapmatching.roadsegment.RoadSegment;
import uni.cs4010.project.trafficprediction.mapmatching.roadsegment.RoadSegmentFactory;
import uni.cs4010.project.trafficprediction.mapmatching.roadsegment.RoadSegmentImpl;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by phil on 15.10.16.
 */
public class MapMatcherImplTest {

    private static final Double SPEED_LIMIT = 50.0;
    private static final GpsLocation DUMMY_BASE_NODE = new GpsLocation(0.1,0.2,0L);
    private static final GpsLocation DUMMY_END_NODE = new GpsLocation(0.2,0.1,0L);
    private static final Edge DUMMY_EDGE = new Edge(0L,DUMMY_BASE_NODE,DUMMY_END_NODE);
    private static final RoadSegment DUMMY_SEGMENT = new RoadSegmentImpl(DUMMY_EDGE,SPEED_LIMIT);
    private static final List<RoadSegment> DUMMY_SEGMENTS = Arrays.asList(
            DUMMY_SEGMENT,
            new RoadSegmentImpl(new Edge(1L,DUMMY_BASE_NODE,DUMMY_END_NODE),SPEED_LIMIT),
            new RoadSegmentImpl(new Edge(2L,DUMMY_BASE_NODE,DUMMY_END_NODE),SPEED_LIMIT),
            new RoadSegmentImpl(new Edge(3L,DUMMY_BASE_NODE,DUMMY_END_NODE),SPEED_LIMIT)
    );
    private static final DataRow DUMMY_ROW1 = new DataRow("1", 0, 0, 0L, 0L, 0L, 50, 0, 0);
    private static final DataRow DUMMY_ROW2 = new DataRow("1", 0, 0, 0L, 0L, 0L, 50, 0, 0);
    private static GpsMatcher gpsMatcherStub;
    private static RoadSegmentFactory roadSegmentFactoryStub;
    private static Map<Long,CarTrace> carTraces = new HashMap<>();
    private MapMatcherImpl mapMatcher;

    @BeforeClass
    public static void setUpClass(){

        gpsMatcherStub = mock(GpsMatcher.class);
        when(gpsMatcherStub.getPassedEdges(any(),any())).thenReturn(Collections.singletonList(DUMMY_SEGMENT.getId()));
        carTraces.put(Long.parseLong(DUMMY_ROW1.taxiId),new CarTrace(Arrays.asList(DUMMY_ROW1,DUMMY_ROW2)));

        roadSegmentFactoryStub = mock(RoadSegmentFactory.class);
        when(roadSegmentFactoryStub.getRoadSegments()).thenReturn(DUMMY_SEGMENTS);
    }

    @Before
    public void setUp(){
        mapMatcher = new MapMatcherImpl(gpsMatcherStub, roadSegmentFactoryStub);
    }

    @Test
    public void matchTest(){
        RoadMap result = mapMatcher.match(carTraces);
        assertEquals(1,result.getTotalCars());
        assertEquals(DUMMY_SEGMENTS.size(),result.getRoadSegments().size());
        assertEquals(true,result.getRoadSegment(DUMMY_SEGMENT.getId()).containsCar(1L));
    }
}