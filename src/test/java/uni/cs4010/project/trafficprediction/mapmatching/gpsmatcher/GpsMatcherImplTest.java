package uni.cs4010.project.trafficprediction.mapmatching.gpsmatcher;

import com.graphhopper.matching.GPXFile;
import com.graphhopper.util.GPXEntry;
import javaslang.collection.Seq;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import uni.cs4010.project.trafficprediction.dataset.*;
import uni.cs4010.project.trafficprediction.mapmatching.graphhopper.GraphHopper;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static uni.cs4010.project.trafficprediction.runner.MapMatching.*;
/**
 * Created by phil on 11.10.16.
 */
public class GpsMatcherImplTest {

    private static final double TOLERANCE = 0.35d;
    private static final int NUMBER_DATASETS = 1;

    private static final String GPX_TEST_DIR = "src/test/resources/gpx/";
    private static final List<String> GPX_TEST_SET = Arrays.asList(
            "test_beijing1.gpx", "test_beijing2.gpx","test_beijing3.gpx", "test_beijing4.gpx");
    private static GraphHopper graphHopper;
    private static List<List<GpsLocation>> carTraces;
    private GpsMatcherImpl gpsMatcher;

    @BeforeClass
    public static void setUpClass(){
        graphHopper = new GraphHopper(GRAPHHOPPER_LOCATION,MAP_DIR,MAP_NAME);
        DataImporter dataImporter = new DataImporter(DATASET_DIRECTORY);
        TraceFilter traceFilter = new TraceFilterImpl();
        Seq<DataRow> rows = dataImporter.getDataFilePaths().take(NUMBER_DATASETS).flatMap(dataImporter::getData);
        carTraces = traceFilter.getGpsTraces(rows).values().stream().map(CarTrace::getGpsTrace).collect(Collectors.toList());
    }

    @Before
    public void setUp(){
        gpsMatcher = (GpsMatcherImpl) graphHopper.createGpsMatcher();
    }

    @Test
    public void matchTestIteratively(){
        int numberOfMatched = 0;
        int numberOfMisses = 0;
        int numberOfTraces = 0;

        for(List<GpsLocation> trace : carTraces){

            for(int i=0;i<trace.size()-1;i+=2){
                numberOfTraces++;

                try{
                    gpsMatcher.matchGps(trace.subList(i,i+2));
                    numberOfMatched++;

                }catch (GraphHopperMatchException e){
                    //System.err.println(e.getMessage());
                    numberOfMisses++;
                }

            }
        }
        assertTrue(String.format("Number of misses should be smaller than: [%d]\n" +
                          "Misses: %02f",(int)(numberOfTraces*TOLERANCE),(double)numberOfMisses/(double)numberOfTraces)
                    ,numberOfMisses<(int)(numberOfTraces*TOLERANCE));
        System.out.println(String.format(this.getClass().getSimpleName() +": Entries matched with tolerance %02f", TOLERANCE));

    }

    @Ignore("GpsMatcherImplTest: WholeTrace matching ignored since we are using the iterative matching\n")
    @Test
    public void matchTestWholeTrace(){
        int numberOfMatched = 0;
        int numberOfMisses = 0;
        int numberOfTraces = 0;

        for(List<GpsLocation> trace : carTraces){

            numberOfTraces++;

            try{
                 gpsMatcher.matchGps(trace);
                 numberOfMatched++;

            }catch (GraphHopperMatchException e){
                  //System.err.println(e.getMessage());
                  numberOfMisses++;
            }

        }

        System.out.println(String.format("Number of traces: %d\n" +
                        "Number of misses: %d\n" +
                        "Number of matched: %d\n" +
                        "Tolerance: %d",
                numberOfTraces,numberOfMisses,numberOfMatched,(int)(numberOfTraces*TOLERANCE)));

        assertTrue(String.format("Number of misses should be smaller than: [%d]\n" +
                        "Misses: %02f",(int)(numberOfTraces*TOLERANCE),(double)numberOfMisses/(double)numberOfTraces)
                ,numberOfMisses<(int)(numberOfTraces*TOLERANCE));
    }

    @Test
    public void matchTestWithManualGPXData(){
        int numberOfMatched = 0;
        int numberOfMisses = 0;

        List<GPXEntry> inputGPXEntries = new GPXFile().doImport(GPX_TEST_DIR + GPX_TEST_SET.get(0)).getEntries();
        List<GpsLocation> gpsLocations = inputGPXEntries.stream()
                .map(gpxEntry -> new GpsLocation(gpxEntry.getLon(),gpxEntry.getLat(),gpxEntry.getTime()))
                .collect(Collectors.toList());

            try{
                gpsMatcher.matchGps(gpsLocations);
                numberOfMatched++;

            }catch (IllegalArgumentException ex){
                numberOfMisses++;
            }

        //System.out.println(String.format("Number of misses: %d\n" +
        //        "Number of matched: %d",numberOfMisses,numberOfMatched));

        assertEquals("All Entries should be matched",numberOfMisses,0);
    }
}