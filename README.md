# README #

### Osm Map ###

Download osm map data from http://download.geofabrik.de/


### TimeBucketing ###

Change path variables DATASET_DIRECTORY and OUTPUT_DIRECTORY in runner.TimeBucketing according to your setup.

run runner.TimeBucketing.main()

### MapMatching ###

Change path variables MAP_DIR, MAP_NAME, DATASET_DIRECTORY,OUTPUT_DIR, OUTPUT_FILE in runner.MapMatching according to your setup.

run runner.MapMatching.main()

By adapting MAP_CENTER and SPAN_VECTOR the number of road segments can be adjusted. The higher the span vector the more road segments are selected. If the area is too small some gps coordinates cannot be matched.

The number of files that are read can be reduced by changing line 61 in the main method